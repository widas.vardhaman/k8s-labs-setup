#!/bin/sh

echo "This is for Master node setup."
curl https://docs.projectcalico.org/manifests/calico.yaml -O

echo "initiating the master cluster"
sudo kubeadm init >> join_creads.txt

echo "Calico network policy apply."
kubectl apply -f calico.yaml

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config